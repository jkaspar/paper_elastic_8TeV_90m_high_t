all: paper_elastic_8TeV_90m_epjc.bbl
	pdflatex paper_elastic_8TeV_90m_epjc.tex

paper_elastic_8TeV_90m_epjc.bbl : bibliography.bib
	bibtex paper_elastic_8TeV_90m_epjc.aux

full:
	pdflatex paper_elastic_8TeV_90m_epjc.tex
	bibtex paper_elastic_8TeV_90m_epjc.aux
	pdflatex paper_elastic_8TeV_90m_epjc.tex
	pdflatex paper_elastic_8TeV_90m_epjc.tex
