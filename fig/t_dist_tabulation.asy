import root;
import pad_layout;
import style;

string topDir = "/afs/cern.ch/work/j/jkaspar/work/analyses/elastic/4000GeV/beta90/high_t/";

drawGridDef = true;

TH1_x_max = 1.9;

xSizeDef = 7cm;
ySizeDef = 5cm;

pen p_unc_full = orange * 0.75 + white * 0.25;
//pen p_unc_anal = red;

//----------------------------------------------------------------------------------------------------

void Print(RootObject o)
{
	int N = o.iExec("GetNbinsX");
	for (int bi = 1; bi <= N; ++bi)
	{
		real c = o.rExec("GetBinCenter", bi);
		real v = o.rExec("GetBinContent", bi);
		real u = o.rExec("GetBinError", bi);

		if (c > TH1_x_max)
			continue;

		write(format("bin %u", bi) + format(": center = %.5f", c) + format(", content = %.5f", v) + format(", stat. unc. = %.5f", u));
	}
}

//----------------------------------------------------------------------------------------------------

void DrawUncBoxes(RootObject o, pen p=black)
{
	int N = o.iExec("GetNbinsX");
	for (int bi = 1; bi <= N; ++bi)
	{
		real c = o.rExec("GetBinCenter", bi);
		real w = o.rExec("GetBinWidth", bi);
		real v = o.rExec("GetBinContent", bi);
		real u = o.rExec("GetBinError", bi);

		if (c > TH1_x_max)
			continue;

		if (v > 0)
		{
			filldraw(Scale((c-w/2, v-u))--Scale((c+w/2, v-u))--Scale((c+w/2, v+u))--Scale((c-w/2, v+u))--cycle, p, nullpen);
		}

		write(format("bin %u", bi) + format(": syst unc. %.5f", u));
	}
}

//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

string f = topDir + "tabulation/tabulate.root";

//----------------------------------------------------------------------------------------------------

NewPad("$|t|\ung{GeV^2}$", "$\d\si/\d t \ung{mb/GeV^2}$");
currentpad.xTicks = LeftTicks(0.2, 0.1);
scale(Linear, Log);

AddToLegend("data with statistical uncertainties", mPl+black+5pt);
AddToLegend("systematic uncertainties", mSq+p_unc_full+5pt);

RootObject h_dsdt_syst = RootGetObject(f, "h_dsdt_val_exp_unc_syst");
DrawUncBoxes(h_dsdt_syst, p_unc_full);

RootObject h_dsdt_stat = RootGetObject(f, "h_dsdt_val_exp_unc_stat");
draw(h_dsdt_stat, "eb", black);
Print(h_dsdt_stat);

limits((0.19, 1e-4), (1.9, 2e1), Crop);

AttachLegend();

GShipout(vSkip=1mm, margin=1mm);
