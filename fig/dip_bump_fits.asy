import root;
import pad_layout;
import style;

string topDir = "/afs/cern.ch/work/j/jkaspar/work/analyses/elastic/4000GeV/beta90/high_t/";

string f_fit = topDir + "fit_dip_bump/make_fits.root";
string f_tab = topDir + "tabulation/tabulate.root";

xSizeDef = 7.0cm;
ySizeDef = 5cm;

pen p_unc_full = orange * 0.75 + white * 0.25;

//----------------------------------------------------------------------------------------------------

void DrawUncBoxes(RootObject o, pen p=black)
{
	int N = o.iExec("GetNbinsX");
	for (int bi = 1; bi <= N; ++bi)
	{
		real c = o.rExec("GetBinCenter", bi);
		real w = o.rExec("GetBinWidth", bi);
		real v = o.rExec("GetBinContent", bi);
		real u = o.rExec("GetBinError", bi);

		if (c > TH1_x_max)
			continue;

		if (v > 0)
		{
			filldraw(Scale((c-w/2, v-u))--Scale((c+w/2, v-u))--Scale((c+w/2, v+u))--Scale((c-w/2, v+u))--cycle, p, nullpen);
		}

		write(format("bin %u", bi) + format(": syst unc. %.5f", u));
	}
}

//----------------------------------------------------------------------------------------------------

NewPad("$|t|\ung{GeV^2}$", "$\d\sigma/\d t\ung{mb/GeV^2}$");
scale(Linear, Log);

RootObject h_dsdt_syst = RootGetObject(f_tab, "h_dsdt_val_exp_unc_syst");
DrawUncBoxes(h_dsdt_syst, p_unc_full);

//RootObject h_dsdt_stat = RootGetObject(f_tab, "h_dsdt_val_exp_unc_stat");
//draw(h_dsdt_stat, "eb", cyan+dashed);

draw(RootGetObject(f_fit, "h_dsdt"), "eb", black);
AddToLegend("data with statistical uncertainties", mPl+5pt);

AddToLegend("systematic uncertainties", mSq+p_unc_full+5pt);

draw(RootGetObject(f_fit, "exp2+exp3/central/f_global"), "l", heavygreen+1pt, "global fit");

draw(RootGetObject(f_fit, "local/central/f_dip"), "l", blue+1pt, "local fit: dip");
draw(RootGetObject(f_fit, "local/central/f_bump"), "l", red+1pt, "local fit: bump");

limits((0.4, 7e-3), (1.1, 0.12), Crop);

AttachLegend(BuildLegend(ymargin=0mm, vSkip=-0.5mm));
