import root;
import pad_layout;
import style;

// non-curly theta
texpreamble("\mathchardef\th=\"0112");

string topDir = "/afs/cern.ch/work/j/jkaspar/work/analyses/elastic/4000GeV/beta90/high_t/";

xSizeDef = 7.0cm;
ySizeDef = 5cm;

string f_matrix = topDir + "DS4/systematics_matrix.root";

xTicksDef = LeftTicks(0.5, 0.1);
yTicksDef = RightTicks(1., 0.2);


//----------------------------------------------------------------------------------------------------

NewPad("$|t|\ung{GeV^2}$", "relative $\d\sigma/\d t$ variation $\ung{\%}$");

AddToLegend("<leading effects:");

draw(scale(1, 100), RootGetObject(f_matrix, "contributions/tilt-thx-thy/g_eff_comb1"), blue, "alignment: rotation about beam");
draw(scale(1, 100), RootGetObject(f_matrix, "contributions/opt-m1/g_eff_comb1"), red, "optics: mode 1");
draw(scale(1, 100), RootGetObject(f_matrix, "contributions/opt-m2/g_eff_comb1"), heavygreen, "optics: mode 2");
draw(scale(1, 100), RootGetObject(f_matrix, "contributions/beam-mom/g_eff_comb1"), blue+dashed, "beam momentum");
draw(scale(1, 100), RootGetObject(f_matrix, "contributions/unsm-sigma-x/g_eff_comb1"), heavygreen+dashed, "unfolding: $\th^*_x$ resolution");
draw(scale(1, 100), RootGetObject(f_matrix, "contributions/unsm-model/g_eff_comb1"), red+dashed, "unfolding: model dependence");

AddToLegend("<{\bf envelope of uncertainties}:");
draw(scale(1, +100), RootGetObject(f_matrix, "matrices/all-anal/combined/g_envelope"), black+1pt);
draw(scale(1, -100), RootGetObject(f_matrix, "matrices/all-anal/combined/g_envelope"), black+1pt);
AddToLegend("$\pm 1\un{\si}$", black+1pt);

limits((0, -5), (1.9, +5), Crop);

AttachLegend(NW, NE);

//----------------------------------------------------------------------------------------------------

/*

NewRow();
DrawOne("alig-sh-thx");
DrawOne("alig-sh-thy");
DrawOne("tilt-thx-thy");

NewRow();
DrawOne("opt-m1");
DrawOne("opt-m2");

//y_min = -0.002; y_max = 0.002;
//yTicksDef = RightTicks(0.001, 0.0002);
NewRow();
DrawOne("acc-corr-sigma-unc");
DrawOne("acc-corr-sigma-asym");
DrawOne("acc-corr-non-gauss");

NewRow();
DrawOne("eff-slp");
DrawOne("beam-mom");

NewRow();
DrawOne("unsm-sigma-x");
DrawOne("unsm-sigma-y");
DrawOne("unsm-model");

//y_min = -0.00; y_max = 0.08;
//yTicksDef = RightTicks(0.01, 0.002);
NewRow();
DrawOne("norm");

NewPad(false);
AddToLegend("diagonal 45b -- 56t", red);
AddToLegend("diagonal 45t -- 56b", blue);
AddToLegend("combination 1", heavygreen + 2pt);
AddToLegend("combination 2 (only if uncorrelated)", magenta + 2pt);
AttachLegend();

*/

GShipout(vSkip=1mm);
